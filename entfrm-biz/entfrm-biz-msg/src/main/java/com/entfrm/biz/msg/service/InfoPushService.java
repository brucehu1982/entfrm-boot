package com.entfrm.biz.msg.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.entfrm.biz.msg.entity.InfoPush;

/**
 * @author entfrm
 * @date 2020-05-24 22:26:59
 * @description 消息推送Service接口
 */
public interface InfoPushService extends IService<InfoPush> {
}
