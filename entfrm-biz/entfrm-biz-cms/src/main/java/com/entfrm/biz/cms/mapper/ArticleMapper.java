package com.entfrm.biz.cms.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.entfrm.biz.cms.entity.Article;

/**
 * @author entfrm
 * @date 2020-04-01 10:04:11
 *
 * @description 文章Mapper接口
 */
public interface ArticleMapper extends BaseMapper<Article>{

}
